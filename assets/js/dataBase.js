function DB(){}
var dbName = 'xiansun';
var dbPath = '_doc/xiansun.db';
// 打开/创建数据库
/**
 * 打开新窗口
 * @param {String} dbName	数据库名称
 * @param {String} dbPath    数据库路径(_doc/xiansun.db)
 */
DB.openDB = function(callback){
	var openDb = plus.sqlite.isOpenDatabase({name: dbName,path: dbPath});
	if(!openDb){
		plus.sqlite.openDatabase({
			name: dbName,
			path: dbPath,
			success: function(e){
				console.log('openDatabase success!');
				callback('success');
			},
			fail: function(e){
				console.log('openDatabase failed: '+JSON.stringify(e));
				callback('fail');
			}
		});
	}
}

//创建数据表
/**
 * 打开新窗口
 * @param {String} tbName	数据表名称
 * @param {String} dbName   数据库名称
 * @param {String} fieldStr   数据库名称
 */
DB.createTable = function(tbName,fieldStr,callback){
	plus.sqlite.executeSql({
		name: dbName,
		sql: 'create table if not exists '+tbName+fieldStr,
		success: function(e){
			console.log('executeSql success!');
			callback('success');
		},
		fail: function(e){
			console.log('executeSql failed: '+JSON.stringify(e));
			callback('fail');
		}
	});
}

// 查询SQL语句
DB.selectSQL = function(sqlStr,callback){
	plus.sqlite.selectSql({
		name: dbName,
		sql: sqlStr,
		success: function(data){
			console.log('selectSql success: ');
			callback(data);
			// for(var i in data){
			// 	console.log(JSON.stringify(data[i]));
			// }
		},
		fail: function(e){
			console.log('selectSql failed: '+JSON.stringify(e));
		}
	});
	
}

// 插入数据
DB.insertToSQL = function(sqlStr,callback){
	plus.sqlite.executeSql({
		name: dbName,
		sql: sqlStr,
		success: function(data){
			console.log('selectSql success: ');
			callback('success');
		},
		fail: function(e){
			console.log('selectSql failed: '+JSON.stringify(e));
			callback('fail');
		}
	});
}

// 更新数据
DB.updateSQL = function(sqlStr,callback){
	plus.sqlite.executeSql({
		name: dbName,
		sql: sqlStr,
		success: function(data){
			console.log('selectSql success: ');
			callback('success');
		},
		fail: function(e){
			console.log('selectSql failed: '+JSON.stringify(e));
			callback('fail');
		}
	});
}

// 删除数据
DB.deleteSQL = function(sqlStr,callback){
	plus.sqlite.executeSql({
		name: dbName,
		sql: sqlStr,
		success: function(data){
			console.log('selectSql success: ');
			callback('success');
		},
		fail: function(e){
			console.log('selectSql failed: '+JSON.stringify(e));
			callback('fail');
		}
	});
}

 

// 关闭数据库
DB.closeDB = function(){
	plus.sqlite.closeDatabase({
		name: dbName,
		success: function(e){
			console.log('closeDatabase success!');
		},
		fail: function(e){
			console.log('closeDatabase failed: '+JSON.stringify(e));
		}
	});
}

