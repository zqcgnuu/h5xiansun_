function CommonUtil(){}
//处理缓存
CommonUtil.setStorage = function(key,str) {
	plus.storage.setItem(key,str);
}
CommonUtil.getStorage = function(key){
	return plus.storage.getItem(key);
}

//处理延时
CommonUtil.WaitFor = function(condition, callback, timeout, unitTime) {
    // 设置默认等待时间（循环间隔）
    if(!unitTime || isNaN(unitTime)) {
        unitTime = 100;
    }
    // 设置超时（到达超时则返回）
    if(!timeout || isNaN(timeout)) {
        timeout = 100;
    }
    if(condition && condition()) { // 等待条件成立，则执行回调
        callback();
    } else if(timeout - unitTime <= 0) { // 等待超时，则执行回调
        callback();
    } else { // 设置延时等待操作
        setTimeout(function() {
           CommonUtil.WaitFor(condition, callback, timeout - unitTime, unitTime);
        }, unitTime);
    }
}
// 重启APP
CommonUtil.restartApp = function() {
	plus.runtime.restart();
}
//创建li
CommonUtil.createElementHTML = function(ele,str,con){
	var node=document.createElement(ele);
	var textnode=document.createTextNode(str);
	node.appendChild(textnode);
	document.getElementById(con).appendChild(node);
}

// 处理点击事件
var _openw=null;
var as='pop-in';// 默认窗口动画
/**
 * 打开新窗口
 * @param {String} id	加载的页面地址，也用作窗口标识
 * @param {String} t    页面的标题
 * @param {String} d	文档页面文件名称（doc目录下），不传入则使用页面的标题
 */
CommonUtil.clicked = function (id, t, d){
	if(_openw){return;}  // 防止快速点击
	var ws={
		// scrollIndicator: 'none',
		// scalable: false,
		// popGesture: 'close',
		// backButtonAutoControl: 'close',
		// titleNView: {
		// 	autoBackButton: true,
		// 	backgroundColor: '#00796B',
		// 	titleColor: '#FFFFFF'
		// }
	};
	//t&&(ws.titleNView.titleText=t,d||(d=t.toLowerCase()));
	// d&&(ws.titleNView.buttons=[{
	// 	fontSrc: '_www/helloh5.ttf',
	// 	text: '\ue648',
	// 	fontSize: '22px',
	// 	onclick: 'javascript:openDoc("/doc/'+d+'.html")'
	// }]);
	_openw=plus.webview.create(id, id, ws);
	_openw.addEventListener('loaded', function(){//页面加载完成后才显示
		_openw&&_openw.show(as, null, function(){
			_openw=null;//避免快速点击打开多个页面
		});
	}, false);
	_openw.addEventListener('hide', function(){
		_openw=null;
	}, false);
	_openw.addEventListener('close', function(){//页面关闭后可再次打开
		_openw=null;
	}, false);
}

CommonUtil.getEmStr = function(val,val2){
	if(typeof(val == 'undefined')){
		return val2;
	}else{
		return val;
	}
}

