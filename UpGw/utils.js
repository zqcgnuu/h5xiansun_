var isArray = Function.isArray || function(o) {
	return typeof o === "object" && Object.prototype.toString.call(o) === "[object Array]";
};

function log(info) {
	console.log(info);
}

String.prototype.format = function() {
	var values = arguments;
	return this.replace(/\{(\d+)\}/g, function(match, index) {
		if (values.length > index) {
			return values[index];
		} else {
			return "";
		}
	});
};

function Util() {}
Util.strToBytes = function(str) {
	var arr = [];
	for (var i = 0, j = str.length; i < j; i += 3) {
		var num = parseInt(str.substring(i, i + 2), 16);
		arr.push(num);
	}
	return new Uint8Array(arr);
}
Util.padLeft = function(val, len, charStr) {
	var s = val + '';
	return new Array(len - s.length + 1).join(charStr, '') + s;
}
Util.padRight = function(val, len, charStr) {
	var s = val + '';
	return s + new Array(len - s.length + 1).join(charStr, '');
}
Util.bytesToStr = function(bytes) {
	var str = "";
	for (var i = 0; i < bytes.length; i++) {
		str += this.byteToStr(bytes[i]) + " ";
	}
	return str;
}
Util.byteToStr = function(b) {
	var str = b.toString(16).toUpperCase();
	if (str.length < 2) str = '0' + str;
	return str;
}
Util.sumCheck = function(bytes, start, end) {
	var sum = 0;
	for (var i = start; i <= end; i++) {
		sum = (sum + bytes[i]) & 0xFF;
	}
	return sum
}
Util.byteOfLong = function(val, pos) {
	return (val >> (pos * 8)) & 0x00ff
}

Util.getEnmuStr = function(em,val)
{
	if(em.properties)
	{
		if(em.properties[val]) return em.properties[val].name;
		return "未定义"+val
	}
	else
	{
		return "未定义枚举"+val
	}
}

Util.progressbar = function(container,inc,progress){
	let pro = inc/progress * 100;
	container.progressbar({progress: 0}).show();
	mui(container).progressbar().setProgress(pro);
	if(pro >= 100) mui(container).progressbar().hide();
}



// var aa = Util.strToBytes("68 16");
// var bb = new Uint8Array(10);
// bb.set(aa);
// bb[2] = 0x21;
// var cc = bb.subarray(0, 3); //结束的索引将不会被包括
// console.log(Util.bytesToStr(cc));


function DateUtil(dt) {
	if (dt) {
		let t = typeof(dt);
		switch (t) {
			case 'string':
				this.dt = new Date(dt);
				break;
			default:
				this.dt = dt;
				break;
		}
	} else {
		this.dt == new Date();
	}
}
DateUtil.prototype.addHours = function(h) {
	this.dt.setTime(this.dt.setHours(this.dt.getHours() + h));
	return this;
}
DateUtil.prototype.toStr = function(str) {
	if (str) {
		fmt = str;
	} else {
		fmt = "yyyy-MM-dd HH:mm:ss";
	}
	var o = {
		"M+": this.dt.getMonth() + 1, //月份
		"d+": this.dt.getDate(), //日
		"H+": this.dt.getHours(), //小时
		"m+": this.dt.getMinutes(), //分
		"s+": this.dt.getSeconds(), //秒
		"q+": Math.floor((this.dt.getMonth() + 3) / 3), //季度
		"S": this.dt.getMilliseconds() //毫秒
	};
	if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.dt.getFullYear() + "").substr(4 - RegExp.$1.length));
	for (var k in o) {
		if (new RegExp("(" + k + ")").test(fmt)) {
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
		}
	}
	return fmt;
}
// var now = new Date(); //你已知的时间
// var dt = new DateUtil("2019-10-12 23:00:00");
// log("dt1=" + dt.toStr());
// log("dt2=" + dt.addHours(2).toStr());


// function XBuffer(arr) {
// 	this.pos = 0;
// 	this.bytes = new Uint8Array(arr);
// }
// XBuffer.prototype.getBin1 = function() {
// 	var ret = this.bytes[this.pos];
// 	this.pos++;
// 	return ret;
// }
// XBuffer.prototype.get = function(pos) {
// 	return this.bytes[pos];
// }


function enumeration(namesToValues) { //这个虚拟的构造函数是返回值
	var enumeration = function() {
		throw "Can't Instantiate Enumerations";
	}; //枚举值继承
	var proto = enumeration.prototype = {
		constructor: enumeration, //标识类型
		toString: function() {
			return this.name;
		}, //返回名字
		valueOf: function() {
			return this.value;
		}, //返回值
		toJSON: function() {
			return this.name;
		} //转换为JSON
	};
	enumeration.values = []; //用以存放枚举对象的数组
	//现在创建新类型的实例
	for (name in namesToValues) { //遍历每个值
		var e = inherit(proto); //创建一个代表它的对象
		e.name = name; //给它一个名字
		e.value = namesToValues[name]; //给它一个值
		enumeration[name] = e; //将它设置为构造函数的属性
		enumeration.values.push(e); //将它存储到值数组中
	}
	//一个类方法，用来对类的实例进行迭代
	enumeration.foreach = function(f, c) {
		for (var i = 0; i < this.values.length; i++) {
			f.call(c, this.values[i]);
		}
	}; //返回标识这个新类型的构造函数
	return enumeration;
}
